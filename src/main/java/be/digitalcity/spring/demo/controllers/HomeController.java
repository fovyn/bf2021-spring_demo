package be.digitalcity.spring.demo.controllers;

import java.util.Arrays;
import java.util.List;

import be.digitalcity.spring.demo.models.macros.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import be.digitalcity.spring.demo.models.Article;

@Controller
@RequestMapping(path = {"", "/home"})
public class HomeController {
    private List<Article> articles = Arrays.asList(
        new Article(1, "Article 1", "DESC Art1", 42.2F, true, true),
        new Article(2, "Article 2", "DESC Art1", 42.2F, true, true),
        new Article(3, "Article ", "DESC Art1", 42.2F, true, true),
        new Article(4, "Article 4", "DESC Art1", 42.2F, true, true),
        new Article(5, "Article 5", "DESC Art1", 42.2F, true, true),
        new Article(6, "Article 6", "DESC Art1", 42.2F, true, true),
        new Article(7, "Article 7", "DESC Art1", 42.2F, true, true),
        new Article(8, "Article 8", "DESC Art1", 42.2F, true, true),
        new Article(9, "Article 9", "DESC Art1", 42.2F, true, true),
        new Article(10, "Article 10", "DESC Art1", 42.2F, true, true)
    );
    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    @GetMapping(path = {"", "/", "/login"})
    public String indexAction(Model view) {
        view.addAttribute("title", "Flavian");

        return "home/index";
    }

    @GetMapping(path = {"/list", "/all"})
    public String listAction(Model view) {
        DataTable dt = new DataTable();

        for (Article a : articles) {
            dt.insertRow(a, "name", a.getName());
            dt.insertRow(a, "id", a.getId());
            dt.insertRow(a, "description", a.getDescription());
        }
        view.addAttribute("dt", dt);
        
        return "home/list";
    }

    @GetMapping(path = {"/edit/{id:[0-9]+}"})
    public String editAction(Model view, @PathVariable("id") int id) {
        Article article = articles.stream().filter(a -> a.getId() == id).findFirst().get();

        view.addAttribute("article", article);

        return "home/edit";
    }

    private boolean Demo(Article article) {
        article.setHasReduction(false);

        return false;
    } 
}
